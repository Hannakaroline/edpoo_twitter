package br.ufms.facom.edpoo.tt_search_compress;

import edu.princeton.cs.algs4.BinaryIn;
import edu.princeton.cs.algs4.BinaryOut;
import edu.princeton.cs.algs4.MinPQ;
//test

/**
 *
 * @author Lucas
 */
public class HuffmanEncoder {

  // alphabet size of extended ASCII
  private static final int R = 256;

  // Do not instantiate.
  private HuffmanEncoder() {
  }

  // Huffman trie node
  private static class Node implements Comparable<Node> {

    private final char ch;
    private final int freq;
    private final Node left, right;

    Node(char ch, int freq, Node left, Node right) {
      this.ch = ch;
      this.freq = freq;
      this.left = left;
      this.right = right;
    }

    // is the node a leaf node?
    private boolean isLeaf() {
      assert ((left == null) && (right == null)) || ((left != null) && (right != null));
      return (left == null) && (right == null);
    }

    // compare, based on frequency
    @Override
    public int compareTo(Node that) {
      return this.freq - that.freq;
    }
  }

  /**
   * Reads a sequence of 8-bit bytes from standard input; compresses them using
   * Huffman codes with an 8-bit alphabet; and writes the results to standard
   * output.
   */
  public static void compress(BinaryIn in, BinaryOut out) {
    // read the input
    String s = in.readString();

    char[] input = s.toCharArray();

    // tabulate frequency counts
    int[] freq = new int[R];
    for (int i = 0; i < input.length; i++) {
      freq[input[i]]++;
    }

    // build Huffman trie
    Node root = buildTrie(freq);

    // build code table
    String[] st = new String[R];
    buildCode(st, root, "");

    // print trie for decoder
    writeTrie(root, out);

    // print number of bytes in original uncompressed message
    out.write(input.length);

    // use Huffman code to encode input
    for (int i = 0; i < input.length; i++) {
      if (input[i] < 256) {
        String code = st[input[i]];
        for (int j = 0; j < code.length(); j++) {
          if (code.charAt(j) == '0') {
            out.write(false);
          } else if (code.charAt(j) == '1') {
            out.write(true);
          } else {
            throw new IllegalStateException("Illegal state");
          }
        }
      }

    }

  }

  // build the Huffman trie given frequencies
  private static Node buildTrie(int[] freq) {

    // initialze priority queue with singleton trees
    MinPQ<Node> pq = new MinPQ<Node>();
    for (char i = 0; i < R; i++) {
      if (freq[i] > 0) {
        pq.insert(new Node(i, freq[i], null, null));
      }
    }

    // special case in case there is only one character with a nonzero frequency
    if (pq.size() == 1) {
      if (freq['\0'] == 0) {
        pq.insert(new Node('\0', 0, null, null));
      } else {
        pq.insert(new Node('\1', 0, null, null));
      }
    }

    // merge two smallest trees
    while (pq.size() > 1) {
      Node left = pq.delMin();
      Node right = pq.delMin();
      Node parent = new Node('\0', left.freq + right.freq, left, right);
      pq.insert(parent);
    }
    return pq.delMin();
  }

  // write bitstring-encoded trie to standard output
  private static void writeTrie(Node x, BinaryOut out) {
    if (x.isLeaf()) {
      out.write(true);
      out.write(x.ch, 8);
      return;
    }

    out.write(false);
    writeTrie(x.left, out);
    writeTrie(x.right, out);
  }

  // make a lookup table from symbols and their encodings
  private static void buildCode(String[] st, Node x, String s) {
    if (!x.isLeaf()) {
      buildCode(st, x.left, s + '0');
      buildCode(st, x.right, s + '1');
    } else {
      st[x.ch] = s;
    }
  }

}
