package br.ufms.facom.edpoo.tt_search_compress;

import edu.princeton.cs.algs4.BinaryIn;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucas
 */
public class DecoderTester {

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    String search;
    String[] tweets;
    String tweetsexpanded;

    try {
      Path path = Paths.get("busca");
      //Path path = Paths.get(args[0]);
      BinaryIn in = new BinaryIn(new ByteArrayInputStream(Files.readAllBytes(path)));

      System.out.println("Start Decoding");
      tweetsexpanded = HuffmanDecoder.expand(in);
      System.out.println("Finished Decoding");

      System.out.println("Decoded Tweets in one string: ");
      System.out.println(tweetsexpanded);

      System.out.println("Decoded Tweets split per tweet: ");
      tweets = tweetsexpanded.split("\\n");

      for (String s : tweets) {
        System.out.println(s);
      }

      System.out.printf("Input Search Term: ");
      search = input.next();

      // TODO implementar alguma busca das que o professor passou
      // essa não serve
      System.out.println("Tweets containing the search term: ");
      for (String s : tweets) {
        if (s.contains(search)) {
          System.out.println(s);
        }
      }

    } catch (IOException ex) {
      Logger.getLogger(DecoderTester.class.getName()).log(Level.SEVERE, null, ex);
    }

  }
}
