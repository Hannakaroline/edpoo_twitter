package br.ufms.facom.edpoo.tt_search_compress;

import edu.princeton.cs.algs4.BinaryIn;

/**
 *
 * @author Lucas
 */
public class HuffmanDecoder {

  // alphabet size of extended ASCII
  private static final int R = 256;

  // Do not instantiate.
  private HuffmanDecoder() {
  }

  // Huffman trie node
  private static class Node implements Comparable<Node> {

    private final char ch;
    private final int freq;
    private final Node left, right;

    Node(char ch, int freq, Node left, Node right) {
      this.ch = ch;
      this.freq = freq;
      this.left = left;
      this.right = right;
    }

    // is the node a leaf node?
    private boolean isLeaf() {
      assert ((left == null) && (right == null)) || ((left != null) && (right != null));
      return (left == null) && (right == null);
    }

    // compare, based on frequency
    @Override
    public int compareTo(Node that) {
      return this.freq - that.freq;
    }
  }

  /**
   * Reads a sequence of bits that represents a Huffman-compressed message from
   * standard input; expands them; and writes the results to standard output.
   *
   * @param in
   * @return
   */
  public static String expand(BinaryIn in) {
    String string;

    // read in Huffman trie from input stream
    Node root = readTrie(in);

    // number of bytes to write
    int length = in.readInt();
    byte[] bytes = new byte[length];
    // decode using the Huffman trie
    for (int i = 0; i < length; i++) {
      Node x = root;
      while (!x.isLeaf()) {
        boolean bit = in.readBoolean();
        if (bit) {
          x = x.right;
        } else {
          x = x.left;
        }
      }

      bytes[i] = (byte) x.ch;

    }
    string = new String(bytes);

    return string;
  }

  private static Node readTrie(BinaryIn in) {
    boolean isLeaf = in.readBoolean();
    if (isLeaf) {
      return new Node(in.readChar(), -1, null, null);
    } else {
      return new Node('\0', -1, readTrie(in), readTrie(in));
    }
  }
}
